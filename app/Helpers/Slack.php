<?php

namespace App\Helpers;

use Cache;
use Storage;

class Slack
{
    public static function channelInfo($channel = null)
    {
        // Cache::forget('slack.channels');
        $channels = Cache::get('slack.channels', function () {
            return collect(json_decode(Storage::get('channels.json')))->sortBy('name')->keyBy('name');
        });

        return $channel ? $channels->firstWhere('name', $channel) : $channels;
    }

    public static function userInfo($user = null)
    {
        // Cache::forget('slack.channels');
        $users = Cache::get('slack.users', function () {
            return collect(json_decode(Storage::get('users.json')))->keyBy('id');
        });

        return $user ? $users->get($user) : $users;
    }
}