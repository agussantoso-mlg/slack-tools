<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Helpers\Slack;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return Inertia::render('Messages');
});

Route::get('/messages/{channel}', function ($channel) {
    $messages = Cache::get("slack.messages.$channel", function () use ($channel) {
        $messages = [];
        foreach (Storage::files($channel) as $file) {
            $date = basename($file, '.json');
            $messages[$date] = json_decode(Storage::get($file));
        }

        return $messages;
    });

    return Inertia::render('Messages', [
        'messages' => $messages,
        'users' => Slack::userInfo(),
    ]);
})->name('messages');

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';
